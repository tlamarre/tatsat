export type Airport = { name: string, iataCode: string };
export type AirportType = "small_airport"
  | "closed"
  | "seaplane_base"
  | "medium_airport"
  | "heliport"
  | "large_airport";
