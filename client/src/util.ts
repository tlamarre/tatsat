import type leaflet from "leaflet";

import type { Airport } from "./types";

export function convertDegreesForUrl(deg: number): number {
  // Flask doesn't like query param floats with a negative sign. And % in JS is
  // the remainder operator. I suppose I could have added optional "-" to the
  // Flask route...
  return (((deg + 180) % 360) + 360) % 360;
}

export const apiCalls = {
  getAirportsInBounds: async (
    bounds: leaflet.LatLngBounds,
    iataCodes: string[],
    airportTypes?: string[]
  ): Promise<[number, number, string, string][]> => {
    const sw = bounds.getSouthWest();
    const ne = bounds.getNorthEast();
    const [swLat, swLng, neLat, neLng] = [sw.lat, sw.lng, ne.lat, ne.lng].map(
      (val) => convertDegreesForUrl(val)
    );

    // const queryParamStr = airportTypes ? `?airport_type=${airportTypes.join(",")}` : "";
    const queryParamStr = "?airport_type=large_airport";
    const url = `/api/airport/all_in_bounds/${swLat},${swLng}-${neLat},${neLng}${queryParamStr}`;
    const res = await fetch(url);
    const data = await res.json();
    return data.airports;
  },

  getAirportCharts: async (iataCodes: string[]): Promise<Blob[]> => {
    const iataCodeListStr = iataCodes.join(",");
    const res = await fetch(`/api/airport/charts/${iataCodeListStr}`);
    const data = await res.json();
    const chartIds: string[] = data.charts;

    const blobs = chartIds.map(async (id) => {
      const chartRes = await fetch(`/api/charts/${id}`);
      const chartData = await chartRes.blob();
      return chartData;
    });

    return Promise.all(blobs);
  },

  getAirportSummaries: async (
    iataCodes: string[],
    routeCount?: number,
    start?: Date,
    end?: Date
  ): Promise<any> => {
    const queryParams = [];
    if (routeCount != undefined) queryParams.push(`mtrc=${routeCount}`);
    // if (start != undefined) {
    //   queryParams.push(`start=${}`);
    // }
    // if (end != undefined) {
    //   queryParams.push(`mtrc=${}`);
    // }
    const iataCodeListStr = iataCodes.join(",");
    const queryParamStr = queryParams.length ? `?${queryParams.join("&")}` : "";
    const url = `/api/airport/summaries/${iataCodeListStr}${queryParamStr}`;
    const res = await fetch(url);
    const data = await res.json();
    return data;
  }
}
