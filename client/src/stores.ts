import { writable, Writable } from "svelte/store";
import type { Airport, AirportType } from "./types";

export type RouteResults = {
  [iataCode: string]: {
    airport_code: string,
    fg_airport_code: string,
    passenger_total: number
  }[]
}

export const showAdmin = writable(false);
// export const expandSidebar = writable(false);
export const airportSearchList: Writable<Airport[]> = writable([]);
export const airportFilter: Writable<AirportType[]> = writable(["large_airport"]);
export const searchStatisticsChartUrls: Writable<string[]> = writable([])
export const routeResults: Writable<RouteResults> = writable({});
