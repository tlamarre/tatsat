from flask import Flask, request, redirect
from flasgger import Swagger
import os
# from sqlalchemy import create_engine
from .model import db_session
from . import api

def create_app(test_config=None):
    app = Flask(__name__, static_folder="../../public", static_url_path="/")
    swagger = Swagger(app)

    @app.before_request
    def before_request():
        # db.connect()
        pass

    @app.after_request
    def after_request(response):
        # db.close()
        return response

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db_session.remove()        

    @app.route("/")
    def redirect_to_app():
        return redirect("/app")

    @app.route("/app", defaults={"path": ""})
    @app.route("/app/<path>")
    def serve_client(path):
        print(path)
        return app.send_static_file("index.html")

    app.register_blueprint(api.blueprint)

    return app
