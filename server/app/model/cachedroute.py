from sqlalchemy import Boolean, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from . import Base

class CachedRoute(Base):
    __tablename__ = "cachedroute"
    airport_code = Column(String(16), ForeignKey("airport.iata_code"), primary_key=True, index=True)
    fg_airport_code = Column(String(16), ForeignKey("airport.iata_code"), primary_key=True, index=True)
    passenger_total = Column(Integer, index=True)

    domestic_airport = relationship(
        "Airport",
        back_populates="domestic_cached_routes",
        foreign_keys=[airport_code],
        primaryjoin="CachedRoute.airport_code == Airport.iata_code"
    )

    foreign_airport = relationship(
        "Airport",
        back_populates="foreign_cached_routes",
        foreign_keys=fg_airport_code,
        primaryjoin="CachedRoute.fg_airport_code == Airport.iata_code"
    )

    def __init__(
            self,
            airport_code,
            fg_airport_code,
            passenger_total
        ):
        self.airport_code = airport_code
        self.fg_airport_code = fg_airport_code
        self.passenger_total = passenger_total

    def __repr__(self):
        return f"<CachedRoute {self.route_id} {self.airport_code}-{self.fg_airport_code} passengers={self.passenger_total}>"
