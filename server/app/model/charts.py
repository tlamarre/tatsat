from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, LargeBinary, select, asc
from . import Base, db_session

import io
import pandas
import matplotlib
import matplotlib.pyplot as plot
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from ..model.route import Route

matplotlib.use("Agg")

def test_plot() -> io.BytesIO:
    fig, ax = plot.subplots(figsize=(4,8))
    fig.patch.set_facecolor("#aaf")
    x = [1, 2, 3, 4, 5]
    y = [9, 10, 11, 12, 13]

    ax.bar(x, y, color="#faa")

    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return output.getvalue()

def make_route_histograms(iata_codes):
    histograms = {
        "foreign": {},
        "domestic": {}
    }

    for key, column in [("domestic", Route.airport_code), ("foreign", Route.fg_airport_code)]:
        for i, iata_code in enumerate(iata_codes):
            # TODO: use get_routes()
            query = select(Route).where(column == iata_code)
            query = query.order_by(asc(Route.date))
            result = db_session.execute(query)

            last_year = 0
            hist = []

            # Iterate through routes, building a histogram with a bucket for
            # each year. Depends on routes being sorted by date.
            for row in result:
                route = row[0]

                year = route.date.year
                if year != last_year:
                    # Push a new bucket to the end of the histogram
                    hist.append([year, 0])
                    last_year = year

                hist[-1][1] += 1

            histograms[key][iata_code] = hist

    return histograms

# TODO: all this does is plot the number of routes.
# Instead of incrementing hist by 1, inc by route.total
def plot_route_histograms(histograms):
    inc_fig, inc_ax = plot.subplots(figsize=(8, 2))
    inc_ax.set_title("Foreign airport route count")
    plot.tight_layout(rect=[0, 0, 0.9, 1])

    out_fig, out_ax = plot.subplots(figsize=(8, 2))
    out_ax.set_title("Domestic airport route count")
    plot.tight_layout(rect=[0, 0, 0.9, 1])

    key_fig_ax_list = []

    output_charts=[]

    # Determine if we need to make a "foreign" chart
    for airport_code, hist in histograms["foreign"].items():
        if len(hist):
            key_fig_ax_list.append(("foreign", inc_fig, inc_ax))
            break

    # Determine if we need to make a "domestic" chart
    for airport_code, hist in histograms["domestic"].items():
        if len(hist):
            key_fig_ax_list.append(("domestic", out_fig, out_ax))
            break

    for key, fig, ax in key_fig_ax_list:
        plots = []
        plot_airport_codes = []

        for airport_code, hist in histograms[key].items():
            if len(hist):
                # (comma after `p` to extract the first element of the plot)
                p, = ax.plot([pair[0] for pair in hist], [pair[1] for pair in hist], label=airport_code)
                plots.append(p)
                plot_airport_codes.append(airport_code)

        print(plots)
        print(plot_airport_codes)
        ax.legend(plots, plot_airport_codes, bbox_to_anchor=(1.00, 1.0), loc="upper left")
        output = io.BytesIO()
        FigureCanvas(fig).print_png(output)
        output_charts.append(output.getvalue())

    return output_charts

def plot_route_ranks(histograms):
    pass # TODO

class Chart(Base):
    __tablename__ = "chart"
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, index=True, default=datetime.now)
    png_data = Column(LargeBinary)

    def __init__(
            self,
            png_data,
            timestamp=None
        ):
        if timestamp:
            self.timestamp = timestamp
        self.png_data = png_data

    def __repr__(self):
        return f"<Chart {self.id}>"
