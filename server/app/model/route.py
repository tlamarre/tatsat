from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, asc
from sqlalchemy.orm import relationship
from . import Base, db_session

class Route(Base):
    __tablename__ = "route"
    route_id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, index=True)
    airport_id = Column(Integer)
    airport_code = Column(String(8), ForeignKey("airport.iata_code"), index=True, nullable=False)
    usg_wac = Column(Integer)
    fg_airport_id = Column(Integer)
    fg_airport_code = Column(String(8), ForeignKey("airport.iata_code"), index=True, nullable=False)
    fg_wac = Column(Integer)
    airline_id = Column(Integer)
    carrier = Column(String(8))
    carrier_group = Column(Integer)
    scheduled = Column(Integer)
    charter = Column(Integer)
    total = Column(Integer)

    domestic_airport = relationship("Airport", back_populates="domestic_airport_routes", foreign_keys=[airport_code])
    foreign_airport = relationship("Airport", back_populates="foreign_airport_routes", foreign_keys=[fg_airport_code])

    def __init__(
            self,
            date,
            airport_id,
            airport_code,
            usg_wac,
            fg_airport_id,
            fg_airport_code,
            fg_wac,
            airline_id,
            carrier,
            carrier_group,
            scheduled,
            charter,
            total
        ):
        self.date = date
        self.airport_id = airport_id
        self.airport_code = airport_code
        self.usg_wac = usg_wac
        self.fg_airport_id = fg_airport_id
        self.fg_airport_code = fg_airport_code
        self.fg_wac = fg_wac
        self.airline_id = airline_id
        self.carrier = carrier
        self.carrier_group = carrier_group
        self.scheduled = scheduled
        self.charter = charter
        self.total = total

    def __repr__(self):
        return f"<Route {self.route_id} {self.airport_code}-{self.fg_airport_code}>"

    def to_plain_dict(self):
        d = vars(self)
        d.pop("_sa_instance_state", None)
        return vars(self)

def route_dict_from_row(row: dict):
    return {
        "date": datetime.strptime(row["data_dte"], "%m/%d/%Y"),
        "airport_id": row["usg_apt_id"],
        "airport_code": row["usg_apt"],
        "usg_wac": row["usg_wac"],
        "fg_airport_id": row["fg_apt_id"],
        "fg_airport_code": row["fg_apt"],
        "fg_wac": row["fg_wac"],
        "airline_id": row["airlineid"],
        "carrier": row["carrier"],
        "carrier_group": row["carriergroup"],
        "scheduled": row["Scheduled"],
        "charter": row["Charter"],
        "total": row["Total"]
    }

def get_routes(iata_codes, start=None, end=None, columns=None):
    start_cond = True if start is None else (Route.date >= start)
    end_cond = True if end is None else (Route.date <= end)

    condition = (
        (Route.airport_code.in_(iata_codes) | Route.fg_airport_code.in_(iata_codes))
            & start_cond
            & end_cond
    )

    query = (
        # TODO: use columns param
        db_session.query(
                Route.route_id,
                Route.date,
                Route.airport_code,
                Route.fg_airport_code,
                Route.total
            )
        .filter(condition)
        .order_by(asc(Route.date))
    )

    result = db_session.execute(query).all()
    return result

