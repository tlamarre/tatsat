import traceback
from sqlalchemy import Boolean, Column, Integer, String, DateTime, Float
from sqlalchemy.orm import relationship
import pandas as pd
import numpy as np
from . import Base

class Airport(Base):
    __tablename__ = "airport"
    airport_id = Column(Integer, primary_key=True, index=True)
    airport_identifier = Column(String(16))
    airport_type = Column(String(32))
    name = Column(String(128))
    latitude = Column(Float())
    longitude = Column(Float())
    elevation = Column(Integer())
    continent = Column(String(8))
    country = Column(String(8))
    region = Column(String(16))
    municipality = Column(String(64))
    scheduled_service = Column(Boolean)
    gps_code = Column(String(16))
    iata_code = Column(String(16), index=True, unique=True)
    home_link = Column(String(512))
    wikipedia_link = Column(String(512))
    keywords = Column(String(128))

    domestic_airport_routes = relationship("Route", back_populates="domestic_airport", foreign_keys="[Route.airport_code]")
    foreign_airport_routes = relationship("Route", back_populates="foreign_airport", foreign_keys="[Route.fg_airport_code]")

    domestic_cached_routes = relationship(
        "CachedRoute",
        back_populates="domestic_airport",
        foreign_keys="[CachedRoute.airport_code]"
    )

    foreign_cached_routes = relationship(
        "CachedRoute",
        back_populates="foreign_airport",
        foreign_keys="[CachedRoute.fg_airport_code]"
    )

    def __init__(self,
                 airport_identifier,
                 airport_type,
                 name,
                 latitude,
                 longitude,
                 elevation,
                 continent,
                 country,
                 region,
                 municipality,
                 scheduled_service,
                 gps_code,
                 iata_code,
                 home_link,
                 wikipedia_link,
                 keywords):
        self.airport_identifier = airport_identifier
        self.airport_type = airport_type
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.elevation = elevation
        self.continent = continent
        self.country = country
        self.region = region
        self.municipality = municipality
        self.scheduled_service = scheduled_service
        self.gps_code = gps_code
        self.iata_code = iata_code
        self.home_link = home_link
        self.wikipedia_link = wikipedia_link
        self.keywords = keywords

    def __repr__(self):
        return f"<Airport {self.name}>"

def airport_dict_from_row(row: dict):
    return {
        "airport_identifier": row["ident"],
        "airport_type": row["type"],
        "name": row["name"],
        "latitude": row["latitude_deg"],
        "longitude": row["longitude_deg"],
        "elevation": row["elevation_ft"],
        "continent": row["continent"],
        "country": row["iso_country"],
        "region": row["iso_region"],
        "municipality": row["municipality"],
        "scheduled_service": row["scheduled_service"] == "yes",
        "gps_code": row["gps_code"],
        "iata_code": row["iata_code"],
        "home_link": row["home_link"],
        "wikipedia_link": row["wikipedia_link"],
        "keywords": row["keywords"],
    }

def compute_most_traveled_routes(iata_codes, routes, route_count):
    """Given a list of airport codes and a DB query result containing all
    routes of interest, rank the routes by total number of passengers, for each
    airport."""
    df = pd.DataFrame(routes) # Let pandas get columns from namedtuple

    # For each airport, find the airports with the most passengers coming to or
    # from the given airport:
    most_traveled_routes = { }
    for code in iata_codes:
        st = None
        try:
            df_filtered = df[(df.airport_code == code) | (df.fg_airport_code == code)]
            pt = df_filtered.pivot_table(
                index="date",
                columns=["airport_code", "fg_airport_code"],
                values="total",
                aggfunc=np.sum
            )
            st = pt.apply(np.sum).sort_values(ascending=False).head(route_count)

            # Iterate over routes to build a dict to pass to client:
            mtrs = []
            for (airport_code, fg_airport_code), value in st.items():
                mtrs.append(
                    {
                        "airport_code": airport_code,
                        "fg_airport_code": fg_airport_code,
                        "passenger_total": value,
                    }
                )

            # Eventually save the computed route totals to database:
            # TODO: Do something with cached routes
            # TODO: Save date range and other query params along with it, otherwise it's useless
            # stmt = (
            #     insert(CachedRoute)
            #     .values(mtrs)
            #     .on_conflict_do_nothing(index_elements=["airport_code", "fg_airport_code"])
            # )
            # model.db_session.execute(stmt)
            # model.db_session.commit()
            # model.db_session.flush()

            most_traveled_routes[code] = mtrs
        except Exception:
            traceback.print_exc()

    return most_traveled_routes
