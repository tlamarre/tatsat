import csv
import datetime
import time

from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


DB_URL =  "sqlite:///./tatsat.db"

engine = create_engine(DB_URL)
# TODO: is there a reason I set autoflush/autocommit = False?
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

ROUTE_DATA_FILE = "/home/tom/documents/air-traffic-data/International_Report_Passengers.csv"
AIRPORT_DATA_FILE = "/home/tom/documents/air-traffic-data/airports-no-dup-iata.csv"

def init_db():
    # TODO: Learn reason these can't be top-level imports. (Circular imports?)
    from . import airport
    from . import cachedroute
    from . import charts
    from . import route
    Base.metadata.create_all(bind=engine)

def drop_tables():
    from . import airport
    from . import cachedroute
    from . import charts
    from . import route
    Base.metadata.drop_all(bind=engine)
    print("Dropped tables")

def load_routes():
    from .route import Route, route_dict_from_row
    t0 = time.time()
    CHUNK_SIZE = 100

    with open(ROUTE_DATA_FILE, newline="") as file:
        file_reader = csv.DictReader(file)
        routes = []

        for i, row in enumerate(file_reader):
            f = route_dict_from_row(row)
            routes.append(f)

            if (i + 1) % CHUNK_SIZE == 0:
                db_session.bulk_insert_mappings(Route, routes)
                routes = []

        else:
            # Insert the routes remaining after the last chunk
            db_session.bulk_insert_mappings(Route, routes)

        t1 = time.time()
        db_session.commit()
        t2 = time.time()
        print("done:", [t1 - t0, t2 - t0])

def load_airports():
    t0 = time.time()
    from .airport import Airport
    CHUNK_SIZE = 100

    forbidden_iata_codes = [
        "",
        "0"
    ]

    with open(AIRPORT_DATA_FILE, newline="") as file:
        file_reader = csv.DictReader(file)
        airports = []
        skipped = 0

        for i, row in enumerate(file_reader):
            if str(row["iata_code"]) not in forbidden_iata_codes:
                a = airport.airport_dict_from_row(row)
                airports.append(a)
            else:
                skipped += 1

            if (i + 1) % CHUNK_SIZE == 0:
                db_session.bulk_insert_mappings(Airport, airports)
                airports = []

        else:
            # Insert the airports remaining after the last chunk
            db_session.bulk_insert_mappings(Airport, airports)
            print("skipped:", skipped)

        t1 = time.time()
        db_session.commit()
        t2 = time.time()
        print("done:", [t1 - t0, t2 - t0])

def check_airport_codes():
    seen_codes = set()
    dup_codes = set()
    with open(AIRPORT_DATA_FILE, newline="") as file:
        file_reader = csv.DictReader(file)
        for row in file_reader:
            code = str(row["iata_code"])
            if code in seen_codes:
                dup_codes.add(code)
            else:
                seen_codes.add(code)
    print(dup_codes)
