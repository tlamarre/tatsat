import io
from flask import Blueprint, send_file
from sqlalchemy import select, asc
from .. import model
from ..model.charts import Chart

blueprint = Blueprint("charts", __name__, url_prefix="/charts")

@blueprint.route("/<int:chart_id>")
def get_chart(chart_id):
    query = select(Chart).where(Chart.id == chart_id)
    result = model.db_session.execute(query).all()
    png_data = result[0][0].png_data

    return send_file(
        io.BytesIO(png_data),
        attachment_filename=f"{chart_id}.png",
        mimetype="image/png"
    )
