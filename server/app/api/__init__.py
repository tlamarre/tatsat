from flask import Blueprint, request
from datetime import datetime

from .. import model
from . import admin
from . import airport
from . import charts
from . import route

blueprint = Blueprint('api', __name__, url_prefix='/api')
blueprint.register_blueprint(admin.blueprint)
blueprint.register_blueprint(airport.blueprint)
blueprint.register_blueprint(charts.blueprint)
blueprint.register_blueprint(route.blueprint)
