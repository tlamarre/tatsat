from os import environ
from flask import Blueprint
from .. import model

blueprint = Blueprint("admin", __name__, url_prefix="/admin")

def running_in_production():
    env = environ["FLASK_ENV"]
    return env != "development"

@blueprint.route("/init")
def create_tables() -> dict:
    if running_in_production(): return { "Success": False }
    model.init_db()
    return {
        "success": True
    }

@blueprint.route("/load_routes")
def load_routes() -> dict:
    if running_in_production(): return { "Success": False }
    model.load_routes()
    return {
        "success": True
    }

@blueprint.route("/load_airports")
def load_airports() -> dict:
    if running_in_production(): return { "Success": False }
    model.load_airports()
    return {
        "success": True
    }

@blueprint.route("/drop_tables")
def drop_tables() -> dict:
    if running_in_production(): return { "Success": False }
    model.drop_tables()
    return {
        "success": True
    }

@blueprint.route("/check_airports")
def check_airports():
    if running_in_production(): return { "Success": False }
    model.check_airport_codes()
    return {
        "success": True
    }
