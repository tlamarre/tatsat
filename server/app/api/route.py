import traceback
from flask import Blueprint, request
from sqlalchemy import select, asc
from .. import model
from ..model.airport import Airport
from ..model.route import Route

blueprint = Blueprint("route", __name__, url_prefix="/route")

@blueprint.route("/list/<string:iata_code>")
def all_airport_routes(iata_code: str) -> dict:
    try:
        limit = int(request.args.get("limit") or 0)
        dom_query = select(Route).where(Route.airport_code == iata_code)
        fg_query = select(Route).where(Route.fg_airport_code == iata_code)

        if limit:
            dom_query = dom_query.limit(limit)
            fg_query = fg_query.limit(limit)

        dom_result = model.db_session.execute(dom_query)
        fg_result = model.db_session.execute(fg_query)

        dom_route_summaries = [r[0].to_plain_dict() for r in dom_result]
        fg_route_summaries = [r[0].to_plain_dict() for r in fg_result]

        return {
            "query": {
                "iataCode": iata_code,
                "limit": limit,
            },
            "domesticAirportRoutes": dom_route_summaries,
            "foreignAirportRoutes": fg_route_summaries,
            "success": True
        }

    except Exception as err:
        return {
            "errType": type(err),
            "errMsg": str(err),
            "success": False
        }, 500
