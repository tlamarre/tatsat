import random
import traceback
from flask import Blueprint, request
from flasgger import swag_from
from sqlalchemy import select, asc
from sqlalchemy.dialects.sqlite import insert

from ..util import parse_month_year_str
from .. import model
from ..model.airport import Airport, compute_most_traveled_routes
from ..model.cachedroute import CachedRoute
from ..model.charts import Chart, make_route_histograms, plot_route_histograms, plot_route_ranks
from ..model.route import Route, get_routes

blueprint = Blueprint("airport", __name__, url_prefix="/airport")

@blueprint.route("/all_in_bounds/<sw_lat>,<sw_lng>-<ne_lat>,<ne_lng>")
def all_in_bounds(sw_lat, sw_lng, ne_lat, ne_lng) -> dict:
    try:
        sw_lat = float(sw_lat)
        sw_lng = float(sw_lng)#  if sw_lng != "0" else -180.0 # TODO: handle literal edge cases
        ne_lat = float(ne_lat)
        ne_lng = float(ne_lng) if float(ne_lng) > 0.3 else 360
        print(ne_lng)
        airport_type_list_str = request.args.get("airport_type") or None
        airport_types = airport_type_list_str.split(",") if airport_type_list_str != None else []

        query = select(Airport).where(
            sw_lat - 180 < Airport.latitude,
            Airport.latitude < ne_lat - 180,
            sw_lng - 180 < Airport.longitude,
            Airport.longitude < ne_lng - 180
        )

        if len(airport_types):
            query = query.where(Airport.airport_type.in_(airport_types))

        query = query.limit(500)
        result = model.db_session.execute(query)

        airports = [row[0] for row in result.all()]
        airport_summaries = [(ap.latitude, ap.longitude, ap.name, ap.iata_code) for ap in airports]

        # TODO: better conversion back to 180 e/w from 360 degrees
        return {
            "bounds": [sw_lat - 180, sw_lng - 180, ne_lat - 180, ne_lng - 180],
            "airports": airport_summaries,
        }

    except Exception as err:
        traceback.print_exc()
        return {
            "success": False
        }

@blueprint.route("/charts/<string:iata_code_list_str>")
def airport_route_charts(iata_code_list_str: str) -> dict:
    try:
        iata_codes = iata_code_list_str.split(",")
        histograms = make_route_histograms(iata_codes)
        png_blobs = plot_route_histograms(histograms)

        ids = []
        for blob in png_blobs:
            c = Chart(blob)
            model.db_session.add(c)
            model.db_session.commit()
            ids.append(c.id)

        return {
            "query": {
                "iataCodes": iata_codes
            },
            "histograms": histograms,
            "charts": ids
        }

    except Exception as err:
        traceback.print_exc()
        return {
            "errType": str(err.__class__.__name__),
            "errMsg": str(err),
            "success": False
        }, 500

@blueprint.route("/summaries/<string:iata_code_list_str>")
@swag_from("./airport_summaries.yml")
def airport_route_summaries(iata_code_list_str: str) -> dict:
    """Get DataFrames describing the route data on a list of airports."""
    try:
        iata_codes = iata_code_list_str.split(",")
        route_count = int(request.args.get("mtrc") or 5)
        start_date = None
        end_date = None

        if (d := request.args.get("start")) is not None:
            try:
                start_date = parse_month_year_str(d)
            except Exception as err:
                print("Error parsing start:")
                traceback.print_exc()

        if (d := request.args.get("end")) is not None:
            try:
                end_date = parse_month_year_str(d)
            except Exception as err:
                print("Error parsing end:")
                traceback.print_exc()

        routes = get_routes(iata_codes, start_date, end_date)
        most_traveled_routes = compute_most_traveled_routes(iata_codes, routes, route_count)

        return {
            "query": {
                "iataCodes": iata_codes,
                "routeCount": route_count,
                "start": start_date,
                "end": end_date
            },
            "mostTraveledRoutes": most_traveled_routes
        }

    except Exception as err:
        traceback.print_exc()
        return {
            "errType": str(err.__class__.__name__),
            "errMsg": str(err),
            "success": False
        }, 500
