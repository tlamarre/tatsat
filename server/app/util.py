from datetime import datetime

def parse_month_year_str(d):
    return datetime.strptime(d, "%m-%Y")
